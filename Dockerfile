FROM ubuntu:20.04

ENV DEBIAN_FRONTEND=noninteractive 

RUN apt update

RUN apt install -y --no-install-recommends make g++ ca-certificates git cmake qt5-default

RUN git clone https://gitlab.com/Someoneb100/azrs-tracking.git /usr/src/LCS

WORKDIR /usr/src/LCS

RUN git checkout develop

RUN ./run.sh

CMD if [ "$(git pull)" != "Already up to date.\n" ]; then cd build && make && cd ..; fi && bin/Logic_Circuit_Simulator 
