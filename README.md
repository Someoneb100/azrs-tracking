# Project 21-Logic-circuit-simulator

Graficko korisnicki interfejs za projektovanje i simulaciju logickih kola.

## Developers

- [Ana Miloradovic, 487/2017](https://gitlab.com/ana.miloradovic)
- [Petar Djordjevic, 353/2020](https://gitlab.com/Someoneb100)
- [Danilo Vuckovic, 87/2015](https://gitlab.com/danilo1996)
- [Ivan Mihajlovic, 221/2016](https://gitlab.com/IvanMihajlovic)
- [Radisa Mitrovic, 429/2019](https://gitlab.com/mowgliluciano)
