#!/bin/bash

root=$(git rev-parse --show-toplevel)
branch=$(git branch 2> /dev/null | awk '{if($1 == "*") print $2;}')
files=$(git ls-tree -r $branch --name-only $root | grep -E '\.((cpp)|(h)|(hpp))$' | sed '/tests/d')

arg=""

if [ "$1" = "-i" ]
then
	arg="$1"
fi

for file in $files
do
	num=$(clang-format -style=file -output-replacements-xml $file | grep -c '<replacement ')
	if [ "$num" != "0" ]
        then
                path=$(realpath --relative-to="$root" "$file")
                echo "Formating: $path"
		clang-format $arg --style=file $file
        fi
done

echo "Done"
