#!/bin/bash

set -e

root=$(git rev-parse --show-toplevel)
branch=$(git branch 2> /dev/null | awk '{if($1 == "*") print $2;}')
files=$(git ls-tree -r $branch --name-only $root | grep -E '\.((cpp)|(h)|(hpp))$' | sed '/tests/d')

for file in $files
do
	sed -i -e "s/^\(\s*\/\/\\)\(.*\)$/\1 AZRS \<3\n\1\2/g" circuit.hpp $file
done

